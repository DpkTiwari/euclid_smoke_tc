package com.euclidemerlad.testcases;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.euclidemerlad.base.BaseClass;
import com.euclidemerlad.pageobjects.E2020Page;
import com.euclidemerlad.pageobjects.HomePage;
import com.euclidemerlad.pageobjects.LoginPage;
import com.euclidemerlad.utility.Log;

public class E2020PageTest extends BaseClass {
	private LoginPage loginPage;
	private HomePage homePage;
	private E2020Page e2020Page;

	@Test(priority = 0,groups = { "Smoke", "Sainity" })
	public void verifygetCurrUrl() throws Throwable {
		loginPage = new LoginPage();
		homePage = new HomePage();
		e2020Page = new E2020Page();
		Log.startTestCase("verifyE2020Page");
		Log.info("User trying to login using Property");
		loginPage = new LoginPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		Thread.sleep(2000);
		Log.info("User trying to go to E2020 Page");
		e2020Page = homePage.clickOnEmerald2020();
		Thread.sleep(2000);
		String actualURL = e2020Page.getCurrUrl();
		String expectedURL = "https://euclidsys--e2020.lightning.force.com/lightning/n/Emerald2020";
		Assert.assertEquals(actualURL, expectedURL);
		Thread.sleep(2000);
		homePage.clickOnAccount();
		Thread.sleep(2000);
		homePage.clickOnAddProduct();
		Thread.sleep(2000);
		Log.endTestCase("verifyE2020Page");

	}
	
//	@Test(priority = 1,groups = {"Smoke","Sainity"})
//	public void Emerald2020() throws Throwable {
//		homePage = new HomePage();
//		Log.startTestCase("TestE2020Page");
//		Log.info("User trying to enter account number");
//		Log.startTestCase("Add Account");
//		Thread.sleep(2000);
//		homePage.clickOnAccount();
//		Thread.sleep(2000);
//		homePage.clickOnAddProduct();
//		Thread.sleep(2000);
//	}

}
