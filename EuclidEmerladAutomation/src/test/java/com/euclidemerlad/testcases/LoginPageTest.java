/**
 * 
 */
package com.euclidemerlad.testcases;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.euclidemerlad.base.BaseClass;
import com.euclidemerlad.dataprovider.DataProviders;
import com.euclidemerlad.pageobjects.HomePage;
import com.euclidemerlad.pageobjects.LoginPage;
import com.euclidemerlad.utility.Log;

/**
 * @author rajivjha
 *
 */
public class LoginPageTest extends BaseClass {
	LoginPage loginPage;
	HomePage homePage;
	
//	@Parameters("browser")
//	@BeforeMethod(groups = { "Smoke", "Sainity" })
//	public void setup(String browser) {
//		launchApp(browser);
//	}
//
//	@AfterMethod(groups = { "Smoke", "Sainity" })
//	public void teardown() {
//		getDriver().quit();
//	}

	@Test(dataProvider = "credentials", dataProviderClass = DataProviders.class, groups = { "Smoke", "Sainity" })
	//@Test(groups = { "Smoke", "Sainity" })
	public void logintest(String uname, String pswd) throws Throwable {

		Log.startTestCase("=====>logintest");
		Log.info("User trying to login");
		// Object of LoginPage in order to call these method
	    loginPage = new LoginPage();
//		 homePage = loginPage.login(prop.getProperty("username"),
//		 prop.getProperty("password"));
//		 Thread.sleep(2000);
		homePage = loginPage.login(uname, pswd);
		String actualURL = homePage.getHomeCurrUrl();
		System.out.println("========URL======"+actualURL);
		Thread.sleep(2000);
		// String expectedURL ="https://euclidsys--e2020.lightning.force.com/one/one.app";
//			String expectedURL = "https://euclidsys--e2020.lightning.force.com/lightning/page/home";
//			Assert.assertEquals(actualURL, expectedURL);
		Assert.assertTrue(actualURL.contains("https://euclidsys--e2020.lightning.force.com"));
		Log.endTestCase("logintest");

	}
}
