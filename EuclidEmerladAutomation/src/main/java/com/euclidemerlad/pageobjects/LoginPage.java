package com.euclidemerlad.pageobjects;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.euclidemerlad.actiondriver.Action;
import com.euclidemerlad.base.BaseClass;

/**
 * @author rajivjha
 *
 */ 

public class LoginPage extends BaseClass {
	
Action action= new Action();
	
	@FindBy(id="username")
	private WebElement userName;
	
	@FindBy(id="password")
	private WebElement password;
	
	@FindBy(name="Login")
	private WebElement Login;
	

	
	public HomePage login(String uname, String pswd) throws Throwable {
		action.scrollByVisibilityOfElement(getDriver(), userName);
		action.type(userName, uname);
		action.type(password, pswd);
		action.click(getDriver(), Login);
		Thread.sleep(2000);
		return new HomePage();
	}
			
	public LoginPage() {
		PageFactory.initElements(getDriver(), this);
	}
			
}
