/**
 * 
 */
package com.euclidemerlad.pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.euclidemerlad.actiondriver.*;
import com.euclidemerlad.base.BaseClass;

/**
 * @author rajivj
 *
 */
public class E2020Page extends BaseClass{
	
	Action action = new Action();
	
	
	@FindBy(xpath = "//span[contains(text(),'Emerald2020')]")
	private WebElement Emerald2020;
	
	@FindBy(className = "slds-input accPatient slds-show ui-autocomplete-input")
	private WebElement Account_No; 

	@FindBy(id = "addOrderLine")
	private WebElement AddProduct;
	
	@FindBy(xpath = "//span[contains(text(),'PSR Review Page')]")
	private WebElement PSR_Review_Page;
	
	public String getCurrUrl() {
		String E2020PageURL = getDriver().getCurrentUrl();
		return E2020PageURL;
	}
	
//	public boolean validateCompany() throws Throwable {
	//	return action.isDisplayed(getDriver(), Company);	
	//}
	
	public boolean validateAccountNo() throws Throwable {
		return action.isDisplayed(getDriver(), Account_No);	
	}
	
	

	public boolean validateAddProduct() throws Throwable {
		return action.isDisplayed(getDriver(), AddProduct);	
	}
	
	
	
	public E2020Page() {
		PageFactory.initElements(getDriver(), this);
	}

}


