/**
 * 
 */
package com.euclidemerlad.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.euclidemerlad.actiondriver.Action;
import com.euclidemerlad.base.BaseClass;

/**
 * @author rajivjha
 *
 */
public class HomePage extends BaseClass {
	
	public String getHomeCurrUrl() {
		String homePageURL = getDriver().getCurrentUrl();
		return homePageURL;
	}
	
	Action action= new Action();
		
	@FindBy(xpath = "//span[contains(text(),'Emerald2020')]")
	private WebElement Emerald2020;
	
	@FindBy(className = "slds-input accPatient slds-show ui-autocomplete-input")
	private WebElement Account_No; 
	
//	@FindBy(id = "j_id0:j_id1:j_id74:LensAccount")
//	private WebElement AccountNo;
	
	@FindBy(xpath = "//body/div[4]/div[1]/section[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]")
	private WebElement body;
	
	@FindBy(id = "addOrderLine")
	private WebElement AddProduct;
	
	@FindBy(xpath = "//span[contains(text(),'PSR Review Page')]")
	private WebElement PSR_Review_Page;
	

	public E2020Page clickOnEmerald2020() throws Throwable { 
	    action.JSClick(getDriver(), Emerald2020);
		Thread.sleep(2000);
        return new E2020Page();
	}
	
	public E2020Page clickOnAccount() throws Throwable {
		action.Alert(getDriver());
		action.moveToElement(getDriver(), Account_No);
		Thread.sleep(4000);
		action.type(Account_No, "2202");
		Thread.sleep(4000);
		action.JSClick(getDriver(), body);
		Thread.sleep(2000);
		return new E2020Page();
	}
	
	public E2020Page clickOnAddProduct() throws Throwable {
		action.scrollByVisibilityOfElement(getDriver(), AddProduct);
		Thread.sleep(2000);
		action.JSClick(getDriver(), AddProduct);
		Thread.sleep(2000);
		return new E2020Page();
	}
	
	public PSRReviewPage clickOnPSRReview() throws Throwable {
		action.JSClick(getDriver(), PSR_Review_Page);
		return new PSRReviewPage();
	}
	
	public HomePage() {
		PageFactory.initElements(getDriver(), this);
	}

}
