package com.euclidemerlad.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.euclidemerlad.actiondriver.Action;
import com.euclidemerlad.base.BaseClass;

public class PSRReviewPage extends BaseClass{
	
	Action action =new Action();
	
	@FindBy(xpath = "//span[contains(text(),'Emerald2020')]")
	private WebElement Emerald2020;
	
	@FindBy(xpath = "//span[contains(text(),'PSR Review Page')]")
	private WebElement PSR_Review_Page;
	
	public String getCurrUrl() {
		String PSRReviewPageURL = getDriver().getCurrentUrl();
		return PSRReviewPageURL;
	}
	
  public PSRReviewPage() {
	PageFactory.initElements(getDriver(), this);
}

}
